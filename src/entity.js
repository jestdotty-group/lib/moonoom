const Components = require('./components')

class Entity{
	static create(...components){
		const unravel = c=> c.constructor === Array? Entity.wrap(...c.map(unravel)): c
		return new Entity(...components.map(unravel))
	}
	static wrap(base, ...components){
		const entity = new Entity(...components)
		entity[Entity.BASE] = base
		return entity
	}
	static toPath(query){return query.split('.')}
	//eg: entity.arm.armour.steel
	// entities with an arm that has armour: arm.armour
	// entities that wear any armour: * armour (one component)
	// entities that have anything of steel: **.steel (zero or more components)
	//take note if you use ** in a query then it must traverse down to the end of entitiy component chains to be sure it didn't miss anything
	static has(entity, componentQuery){
		const path = this.toPath(componentQuery)
		while(path[path.length-1]=='**') path.pop() //if ends in ** remove it

		let matching = [entity]
		for(let i=0; i<path.length; i++){
			const token = path[i]
			matching = (Entity.queries[token] || Entity.queries.unmatched)(matching, token, i, path)
			if(!matching.length) return false
		}
		return true
	}
	constructor(...components){
		if(components.length) this.add(...components)
		return (this.proxy = new Proxy(this, Entity.proxy))
	}
	attach(engine){
		if(this[Entity.ENGINE]) throw new Error('Engine already attached')
		this[Entity.ENGINE] = engine
		this[Entity.ENGINE]._add(this.proxy, ...(this[Entity.COMPONENTS] && this[Entity.COMPONENTS].keys() || []))
		return this
	}
	dettach(){
		if(!this[Entity.ENGINE]) return false
		this[Entity.ENGINE]._remove(this.proxy, true, ...(this[Entity.COMPONENTS] && this[Entity.COMPONENTS].keys() || []))
		return delete this[Entity.ENGINE]
	}
	add(...components){
		if(!this[Entity.COMPONENTS]) this[Entity.COMPONENTS] = new Components()
		components = components.map(component=> component instanceof Entity? component: Entity.wrap(component))
		const added = this[Entity.COMPONENTS].add(...components)
		if(this[Entity.ENGINE]) this[Entity.ENGINE]._add(this.proxy, ...added)
		return components
	}
	remove(...componentsOrNames){
		if(!this[Entity.COMPONENTS]) return false
		const removed = this[Entity.COMPONENTS].remove(...componentsOrNames)
		if(this[Entity.ENGINE]) this[Entity.ENGINE]._remove(this.proxy, false, ...removed)
		return !!removed.length
	}
	has(componentQuery){return Entity.has(this, componentQuery)}
	[Components.IS](base){return this === base || this[Entity.BASE] === base}
}
Entity.BASE = Symbol('base')
Entity.ENGINE = Symbol('engine')
Entity.COMPONENTS = Symbol('components')
Entity.queries = {
	'unmatched': (matching, token)=> matching.map(v=> v[Entity.COMPONENTS] && v[Entity.COMPONENTS].get(token)).filter(v=>!!v),
	'*': matching=> matching.map(v=> v[Entity.COMPONENTS]? [...v[Entity.COMPONENTS].values()]: []).flat(),
	'**': (matching, token, i, path)=>{
		let found = false
		const nextToken = path[i+1]
		do{
			matching = matching
			.map(v=> v[Entity.COMPONENTS]? v[Entity.COMPONENTS].has(nextToken)? found=true && v: [...v[Entity.COMPONENTS].values()]: [])
			.flat()
		}while(matching.length && !found)
		return matching
	}
}
Entity.proxy = {
	get: (target, prop)=>{
		return target[prop] ||
			(target[Entity.COMPONENTS] && target[Entity.COMPONENTS].getComplex(prop)) ||
			(target[Entity.BASE] && target[Entity.BASE][prop])
	},
	set: (target, prop, value)=>{
		if(typeof prop === 'symbol'){
			if(prop === Entity.BASE) target[Components.NAME] = value.constructor.name //trick getKey class name thing in Components
			target[prop] = value
			return true
		}
		if(!target[Entity.BASE]) target[Entity.BASE] = {} //make base object if one doesn't exist
		target[Entity.BASE][prop] = value
		return true
	},
	deleteProperty: (target, prop)=>{
		if(typeof prop === 'symbol') return delete target[prop]
		if(target.remove(prop)) return true
		return target[Entity.BASE] && delete target[Entity.BASE][prop]
	}
}

module.exports = Entity
