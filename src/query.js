const Entity = require('./entity')

//you can reduce search time by specifying the most rare component match(es) first
class Query{
	static getAll(engine){
		const results = new Set()
		engine.components.forEach(entities=> entities.forEach(entity=> results.add(entity)))
		return results
	}
	static getSpecific(engine, term){
		const [first] = Entity.toPath(term)
		return new Set([...(
			first==='**'||first==='*'? this.getAll(engine):
			(engine.components.get(first) || [])
		)].filter(e=> e.has(term)))
	}
	//eg, include=['hat', ['pants', 'shorts'], 'shirt'] = get anyone wearing hat, shirt, and (pants or shorts)
	constructor(include=[], exclude=[]){
		this.include = include
		this.exclude = exclude
	}
	traverse(engine, terms, depth=0){ //even is inclusive and, odd is exclusive or
		let result
		const and = d=>{
			const entities = d instanceof Array? this.traverse(engine, d, depth+1): false
			if(result){
				if(entities){
					result.forEach(entity=> !entities.has(entity) && result.delete(entity)) //if not deeper entities match, remove
				}else result.forEach(entity=> !entity.has(d) && result.delete(entity)) //if not term match, remove
			}else result = entities || Query.getSpecific(engine, d) //initialize result
		}
		const or = d=>{
			const entities = d instanceof Array? this.traverse(engine, d, depth+1): Query.getSpecific(engine, d)
			if(result) entities.forEach(entity=> result.add(entity))
			else result = entities //initialize result
		}
		terms.forEach(depth % 2 === 0? and: or)
		return result
	}
	search(engine){
		const included = this.include.length? this.traverse(engine, this.include): Query.getAll(engine)
		const excluding = this.exclude.length? this.traverse(engine, this.exclude): new Set() //could optimize this probably
		excluding.forEach(entity=> included.delete(entity))
		return [...included]
	}
}

module.exports = Query
