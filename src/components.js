class Components extends Map{
	static getKey(component){ //camelCase class name
		const name = component[this.NAME] || component.constructor.name
		return name[0].toLowerCase() + name.substring(1)
	}
	static isArray(key){return key[key.length - 1] === '$'}
	static getArrayKey(key){return key.substring(0, key.length-1)}
	constructor(...components){
		super()
		this.add(...components)
	}
	add(...components){ //returns names of successfully added
		return components.map(component=>{
			const key = this.constructor.getKey(component)
			if(!super.has(key)) super.set(key, component) //add first
			else if(!(super.get(key).constructor === Array)) //only specifically array type, not subclasses of array
				super.set(key, [super.get(key), component]) //add second
			else super.get(key).push(component) //add subseqeunt
			return key
		})
	}
	removeComponent(component){ //returns name if removed
		const key = this.constructor.getKey(component)
		const value = super.get(key)
		if(!value) return
		if(value.constructor === Array){
			const index = value.findIndex(c=> c[Components.IS] && c[Components.IS](component) || c === component)
			if(index === -1) return //didn't remove anything, so return nothing
			value.splice(index, 1)
			if(value.length === 1) super.set(key, value[0])
			return key
		}else if(value[Components.IS] && value[Components.IS](component) || value === component){
			super.delete(key)
			return key
		}
	}
	removeFirst(key){ //returns name if removed
		const value = super.get(key)
		if(!value) return
		if(value.constructor === Array){
			value.shift()
			if(value.length === 1) super.set(key, value[0])
			return key
		}else{
			super.delete(key)
			return key
		}
	}
	removeAll(key){ //returns names of removed
		const value = super.get(key)
		if(!value) return
		if(value.constructor === Array){
			const length = value.length
			super.delete(key)
			return Array(length).fill(key)
		}else{
			super.delete(key)
			return key
		}
	}
	//component will remove component
	//string will remove the first one
	//string$ will remove all
	remove(...componentsOrNames){ //returns names of successfully removed
		return componentsOrNames.map(componentOrName=>{
			if(typeof componentOrName === 'string'){
				if(this.constructor.isArray(componentOrName))
					return this.removeAll(this.constructor.getArrayKey(componentOrName))
				return this.removeFirst(componentOrName)
			}else return this.removeComponent(componentOrName)
		}).flat().filter(key=> !!key)
	}
	//get first component under name, or if ending in $ or array get an array of all components under name
	getComplex(componentName, array){
		if(this.constructor.isArray(componentName)){
			componentName = this.constructor.getArrayKey(componentName)
			array = true
		}
		if(!super.has(componentName)) return array? []: undefined
		const value = super.get(componentName)
		if(value.constructor === Array) return array? value: value[0]
		return array? [value]: value
	}
}
Components.NAME = Symbol('name')
Components.IS = Symbol('is')

module.exports = Components
