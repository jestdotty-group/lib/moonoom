const assert = require('assert')

const Entity = require('../src/entity')

class Component{}
class Component2{}
class MockEngine{
	constructor(){
		this.added = []
		this.removed = []
	}
	_add(entity, ...components){
		this.added.push([entity, components])
	}
	_remove(entity, force, ...components){
		this.removed.push([entity, force, components])
	}
}

describe('entity', ()=>{
	it('create', ()=>{
		const component = new Component()
		const entity = Entity.create(component)

		assert.equal(entity[Entity.COMPONENTS].size, 1)
		assert.equal(entity[Entity.COMPONENTS].get('component')[Entity.BASE], component)
	})
	it('wrap', ()=>{
		const base = new Component()
		const component = new Component()
		const entity = Entity.wrap(base, component)

		assert.equal(entity[Entity.BASE], base)
		assert.equal(entity[Entity.COMPONENTS].size, 1)
		assert.equal(entity[Entity.COMPONENTS].get('component')[Entity.BASE], component)
	})
	it('shorthand create wrap', ()=>{ //TODO implement
		const base = new Component()
		const component = new Component()
		const entity = Entity.create(component, [base, component], base)

		assert.equal(entity[Entity.COMPONENTS].size, 1)
		const array = entity.component$
		assert.equal(array[0][Entity.BASE], component)
		assert.equal(array[1][Entity.BASE], base)
		assert.equal(array[1][Entity.COMPONENTS].get('component')[Entity.BASE], component)
		assert.equal(array[2][Entity.BASE], base)
	})
	describe('components', ()=>{
		it('add', ()=>{
			const entity = Entity.create()
			assert.strictEqual(entity[Entity.COMPONENTS], undefined)

			const component0 = new Component()
			const [component] = entity.add(component0)
			assert.equal(entity[Entity.COMPONENTS].size, 1)
			assert.notEqual(entity[Entity.COMPONENTS].get('component'), component0)
			assert.equal(entity[Entity.COMPONENTS].get('component'), component)
		})
		it('remove', ()=>{
			const component = new Component()
			const entity = Entity.create(component)
			assert.equal(entity[Entity.COMPONENTS].size, 1)
			assert.equal(entity[Entity.COMPONENTS].get('component')[Entity.BASE], component)

			const removed = entity.remove(component)
			assert.equal(removed, true)
			assert.equal(entity[Entity.COMPONENTS].size, 0)
		})
		describe('has', ()=>{
			describe('component', ()=>{
				it('missing', ()=>{
					const entity = Entity.create()
					assert.equal(entity.has('component'), false)
				})
				it('matching', ()=>{
					const entity = Entity.create(new Component())
					assert.equal(entity.has('component'), true)
				})
			})
			describe('component.component', ()=>{
				it('missing', ()=>{
					const entity = Entity.create()
					assert.equal(entity.has('component.component'), false)
				})
				it('first matching', ()=>{
					const entity = Entity.create(new Component())
					assert.equal(entity.has('component.component'), false)
				})
				it('second matching', ()=>{
					const entity = Entity.create(Entity.wrap(new Component2, new Component()))
					assert.equal(entity.has('component.component'), false)
				})
				it('matching', ()=>{
					const entity = Entity.create(Entity.wrap(new Component, new Component()))
					assert.equal(entity.has('component.component'), true)
				})
			})
			describe('*', ()=>{
				it('* missing', ()=>{
					const entity = Entity.create()
					assert.equal(entity.has('*'), false)
				})
				it('*', ()=>{
					const entity = Entity.create(new Component())
					assert.equal(entity.has('*'), true)
				})
				it('*.component missing', ()=>{
					const entity = Entity.create(Entity.wrap(new Component2, new Component2()))
					assert.equal(entity.has('*.component'), false)
				})
				it('*.component', ()=>{
					const entity = Entity.create(Entity.wrap(new Component2, new Component()))
					assert.equal(entity.has('*.component'), true)
				})
				it('component.* missing', ()=>{
					const entity = Entity.create(Entity.wrap(new Component2, new Component2()))
					assert.equal(entity.has('component.*'), false)
				})
				it('component.*', ()=>{
					const entity = Entity.create(Entity.wrap(new Component, new Component2()))
					assert.equal(entity.has('component.*'), true)
				})
				it('*.* missing', ()=>{
					const entity = Entity.create(new Component)
					assert.equal(entity.has('*.*'), false)
				})
				it('*.*', ()=>{
					const entity = Entity.create(Entity.wrap(new Component, new Component()))
					assert.equal(entity.has('*.*'), true)
				})
			})
			describe('**', ()=>{
				it('** empty', ()=>{
					const entity = Entity.create()
					assert.equal(entity.has('**'), true)
				})
				it('** with component', ()=>{
					const entity = Entity.create(new Component())
					assert.equal(entity.has('**'), true)
				})
				it('**.component empty', ()=>{
					const entity = Entity.create()
					assert.equal(entity.has('**.component'), false)
				})
				it('**.component with component', ()=>{
					const entity = Entity.create(new Component())
					assert.equal(entity.has('**.component'), true)
				})
				it('**.component match', ()=>{
					const entity = Entity.create(Entity.wrap(new Component2(), Entity.wrap(new Component2(), new Component())))
					assert.equal(entity.has('**.component'), true)
				})
				it('component.** empty', ()=>{
					const entity = Entity.create()
					assert.equal(entity.has('component.**'), false)
				})
				it('component.** with component', ()=>{
					const entity = Entity.create(new Component())
					assert.equal(entity.has('component.**'), true)
				})
				it('component.** match', ()=>{
					const entity = Entity.create(Entity.wrap(new Component(), Entity.wrap(new Component2(), new Component())))
					assert.equal(entity.has('component.**'), true)
				})
				it('**.component.**.component empty', ()=>{
					const entity = Entity.create()
					assert.equal(entity.has('**.component.**.component'), false)
				})
				it('**.component.**.component with components', ()=>{
					const entity = Entity.create(Entity.wrap(new Component(), Entity.wrap(new Component2(), new Component())))
					assert.equal(entity.has('**.component.**.component'), true)
				})
				it('**.component.**.component match', ()=>{
					const entity = Entity.create(Entity.wrap(new Component2(), Entity.wrap(new Component(), Entity.wrap(new Component(), new Component()))))
					assert.equal(entity.has('**.component.**.component'), true)
				})
			})
		})
	})
	describe('engine', ()=>{
		it('attach', ()=>{
			const engine = new MockEngine()
			const entity = Entity.create()
			assert.strictEqual(entity, entity.attach(engine))
			assert.equal(entity[Entity.ENGINE], engine)
		})
		it('attach with existing', ()=>{
			const engine = new MockEngine()
			const entity = Entity.create()
			assert.strictEqual(entity, entity.attach(engine))
			assert.throws(()=>{
				entity.attach(new MockEngine())
			}, new Error('Engine already attached'))
		})
		it('dettach', ()=>{
			const entity = Entity.create()
			assert.strictEqual(entity[Entity.ENGINE], undefined)
			const success = entity.dettach()
			assert.equal(success, false)
			assert.equal(entity[Entity.ENGINE], undefined)
		})
		it('dettach with existing', ()=>{
			const engine = new MockEngine()
			const entity = Entity.create()
			assert.strictEqual(entity, entity.attach(engine))
			assert.equal(entity[Entity.ENGINE], engine)
			const success = entity.dettach()
			assert.equal(success, true)
			assert.equal(entity[Entity.ENGINE], undefined)
		})
		describe('components', ()=>{
			it('attach', ()=>{
				const engine = new MockEngine()
				const component = new Component()
				const entity = Entity.create(component)

				assert.strictEqual(entity, entity.attach(engine))
				assert.deepEqual(engine.added, [[entity, ['component']]])
			})
			it('dettach', ()=>{
				const engine = new MockEngine()
				const component = new Component()
				const entity = Entity.create(component)
				assert.strictEqual(entity, entity.attach(engine))
				assert.deepEqual(engine.added, [[entity, ['component']]])

				const success = entity.dettach()
				assert.equal(success, true)
				assert.equal(entity[Entity.ENGINE], undefined)
				assert.deepEqual(engine.removed, [[entity, true, ['component']]])
			})
			it('add', ()=>{
				const engine = new MockEngine()
				const entity = Entity.create()
				assert.strictEqual(entity, entity.attach(engine))

				const component = new Component()
				entity.add(component)
				assert.deepEqual(engine.added, [
					[entity, []],
					[entity, ['component']]
				])
			})
			it('remove', ()=>{
				const engine = new MockEngine()
				const component = new Component()
				const entity = Entity.create(component)
				assert.strictEqual(entity, entity.attach(engine))

				const success = entity.remove(component)
				assert.equal(success, true)
				assert.deepEqual(engine.removed, [[entity, false, ['component']]])
			})
		})
	})
	describe('proxy', ()=>{
		describe('get', ()=>{
			it('entity', ()=>{
				const entity = Entity.create()
				assert.equal(entity[Entity.COMPONENTS], undefined)
			})
			it('component', ()=>{
				const component = new Component()
				const entity = Entity.create(component)
				assert.equal(entity.component[Entity.BASE], component)
			})
			it('base', ()=>{
				const entity = Entity.wrap({x:5})
				assert.equal(entity.x, 5)
			})
			it('not found', ()=>{
				const entity = Entity.create()
				assert.equal(entity.y, undefined)
			})
		})
		describe('set', ()=>{
			it('on existing', ()=>{
				const component = new Component()
				const entity = Entity.wrap(component)
				entity.z = 7
				assert.equal(entity[Entity.BASE].z, 7)
				assert.equal(entity.z, 7)
				assert.equal(component.z, 7)
			})
			it('on empty', ()=>{
				const entity = Entity.create()
				entity.z = 7
				assert.equal(entity[Entity.BASE].z, 7)
				assert.equal(entity.z, 7)
			})
		})
		describe('delete', ()=>{
			it('symbol', ()=>{
				const component = new Component()
				const entity = Entity.wrap(component)
				assert.equal(entity[Entity.BASE], component)
				const success = delete entity[Entity.BASE]
				assert.equal(success, true)
				assert.equal(entity[Entity.BASE], undefined)
			})
			it('component', ()=>{
				const component = new Component()
				const entity = Entity.create(component)
				assert.equal(entity[Entity.COMPONENTS].size, 1)
				assert.equal(entity.component[Entity.BASE], component)
				const success = delete entity.component
				assert.equal(success, true)
				assert.equal(entity[Entity.COMPONENTS].size, 0)
				assert.equal(entity.component, undefined)
			})
			it('base', ()=>{
				const component = new Component()
				component.h = 9
				const entity = Entity.wrap(component)
				assert.equal(entity[Entity.BASE].h, 9)
				assert.equal(entity.h, 9)
				assert.equal(component.h, 9)
				const success = delete entity.h
				assert.equal(success, true)
				assert.equal(entity[Entity.BASE].h, undefined)
				assert.equal(entity.h, undefined)
				assert.equal(component.h, undefined)
			})
			it('not found', ()=>{
				const entity = Entity.create()
				const success = delete entity.j
				assert.equal(success, false)
			})
		})
	})
})
