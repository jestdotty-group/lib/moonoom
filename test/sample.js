const assert = require('assert')
const {Engine, Entity} = require('../index')

//components
class Player{}
class Health{constructor(points){this.points = points}}
class Attack{constructor(points){this.points = points}}
class Attacking{constructor(entity){this.entity = entity}}

//systems
let id = 1
const player = (engine, action)=>{
	if(!action) return
	const player = engine.get('player')[0]
	if(action==='attack'){
		const creatures = engine.search([], ['player'])
		if(creatures.length) player.add(new Attacking(creatures[0]))
	}
}
const AI = engine=>{
	const player = engine.get('player')[0]
	engine.search([], ['player']).forEach(creature=> creature.add(new Attacking(player)))
}
const spawning = engine=>{
	if(engine.search([], ['player']).length) return
	const monster = Entity.create(new Health(4), new Attack(1)).attach(engine)
	monster.id = id++
}
const combat = engine=> engine.get('attacking').forEach(attacker=>{
	const target = attacker.attacking.entity
	delete attacker.attacking
	target.health.points -= attacker.attack.points
	if(target.health.points <= 0) target.dettach()
})

describe('sample', ()=>{
	let engine
	const start = ()=>{
		const player = Entity.create(new Player(), new Health(10), new Attack(2)).attach(engine)
		player.id = id++
	}
	const turn = action=> engine.update('game', action)
	before(()=>{
		id = 1
		engine = new Engine()
		engine.add('game', player, AI, spawning, combat)
		start()
	})
	it('some rounds', ()=>{
		let entities = engine.get()
		assert.deepEqual(entities.map(e=> e.id), [1])
		assert.equal(entities[0].health.points, 10)
		assert.equal(entities[0].attack.points, 2)

		turn()
		entities = engine.get()
		assert.deepEqual(entities.map(e=> e.id), [1, 2])
		assert.equal(entities[0].health.points, 10)
		assert.equal(entities[0].attack.points, 2)
		assert.equal(entities[1].health.points, 4)
		assert.equal(entities[1].attack.points, 1)

		turn('attack')
		entities = engine.get()
		assert.deepEqual(entities.map(e=> e.id), [1, 2])
		assert.equal(entities[0].health.points, 9)
		assert.equal(entities[0].attack.points, 2)
		assert.equal(entities[1].health.points, 2)
		assert.equal(entities[1].attack.points, 1)

		turn('attack')
		entities = engine.get()
		assert.deepEqual(entities.map(e=> e.id), [1])
		assert.equal(entities[0].health.points, 8)
		assert.equal(entities[0].attack.points, 2)

		turn()
		entities = engine.get()
		assert.deepEqual(entities.map(e=> e.id), [1, 3])
		assert.equal(entities[0].health.points, 8)
		assert.equal(entities[0].attack.points, 2)
		assert.equal(entities[1].health.points, 4)
		assert.equal(entities[1].attack.points, 1)
	})
})
