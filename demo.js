const readline = require('readline')
const {Engine, Entity} = require('./index')

const engine = new Engine()
const turn = action=>{
	if(engine.get('player')[0]) engine.update('game', action)
	else engine.update('menu', action)
}

//components
class Player{}
class Health{constructor(points){this.points = points}}
class Attack{constructor(points){this.points = points}}
class Attacking{constructor(entity){this.entity = entity}}
class Name{constructor(id){this.id = id}}
class Event{constructor(description, order=Event.order=(Event.order||0)+1){
	this.description = description
	this.order = order
}}
class Dead{}

//systems
class Menu{
	constructor(){
		this.items = {
			'start': engine=>{
				const entity = Entity.create(new Player(), new Health(10), new Attack(2))
				entity.add(new Name(`Player`))
				entity.attach(engine)
				console.log('CREATED NEW GAME')
				turn()
			}
		}
	}
	update(engine, action){
		const fn = this.items[action]
		if(fn) fn(engine)
		else console.log(`MENU: ${Object.keys(this.items).join(', ')}`)
	}
}
const menu = new Menu()
engine.add('menu', menu.update.bind(menu))

class Control{
	constructor(){
		this.turn = 0
		this.actions = {
			'attack': engine=>{
				const player = engine.get('player')[0]
				const creatures = engine.search(['attack'], ['player'])
				if(creatures.length) player.add(new Attacking(creatures[0]))
				else console.log('THERE IS NOTHING TO ATTACK')
			},
			'wait': ()=> console.log(`TURN: ${this.turn++}`)
		}
	}
	update(engine, action){
		const fn = this.actions[action]
		if(fn) return fn(engine)
		console.log(`YOU CAN: ${Object.keys(this.actions).join(', ')}`)
		this.actions.wait()
	}
}
const control = new Control()
const AI = engine=>{
	const player = engine.get('player')[0]
	engine.search(['attack'], ['player']).forEach(creature=> creature.add(new Attacking(player)))
}
let creatures = 0
const spawning = engine=>{
	if(engine.search(['attack'], ['player']).length) return
	const entity = Entity.create(new Health(4), new Attack(1))
	entity.add(new Name(`Creature[${creatures++}]`))
	entity.attach(engine)
	entity.add(new Event(`spawned with ${entity.health.points} health points and ${entity.attack.points} attack points.`))
}
const combat = engine=> engine.get('attacking').forEach(attacker=>{
	const target = attacker.attacking.entity
	delete attacker.attacking
	target.health.points -= attacker.attack.points
	attacker.add(new Event(`attacks for ${attacker.attack.points}`))
	target.add(new Event(`health reduced to ${target.health.points}`))

	if(target.health.points <= 0){
		target.add(new Dead())
		target.add(new Event(`dead`, Number.MAX_SAFE_INTEGER))
		const prize = -target.health.points //overkill
		if(prize>0){
			attacker.health.points += prize
			attacker.add(new Event(`increased health by ${-target.health.points} to ${attacker.health.points}`))
		}else{
			attacker.attack.points++
			attacker.add(new Event(`increased attack by 1 to ${attacker.attack.points}`))
		}
	}
})
const logging = engine=>{
	const events = []
	engine.get('event').forEach(entity=>{
		events.push(...entity.event$.map(event=>({event, entity})))
		delete entity.event$
	})
	events
		.sort((a, b)=> a.event.order - b.event.order)
		.forEach(({event, entity})=> console.log(`${entity.name? entity.name.id: 'unknown'}: ${event.description}`))
}
const death = engine=> engine.get('dead').forEach(entity=> entity.dettach())
engine.add('game', control.update.bind(control), AI, spawning, combat, logging, death)

//controls
turn()
readline.createInterface({
	input: process.stdin,
	output: process.stdout
}).on('line', line=>{
	const value = line.trim()
	if(value.length) console.log(`>> ${line}`)
	turn(value)
})
